/* eslint-disable import/no-anonymous-default-export */
export default {
  openGraph: {
    type: "website",
    locale: "en_IE",
    site_name: "SIS",
  },
  twitter: {
    handle: "@handle",
    site: "@site",
    cardType: "summary_large_image",
  },
};
