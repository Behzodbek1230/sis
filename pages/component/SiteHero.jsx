import React, { useEffect, useState } from "react";
import Header from "../component/header/Header";
import url from "../url.json";
import Link from "next/link";
export default function SiteHero() {
  const [datas, setDatas] = useState([]);
  useEffect(() => {
    fetch(url.url + "/banner/")
      .then((res) => res.json())
      .then((data) => {
        setDatas(data[0]);
      });
  }, [datas]);
  return (
    <div className="site-hero">
      <Header />
      <div className="hero">
        <h1 className="hero__title">{datas.banner_title}</h1>
        <p className="hero__text">{datas.banner_subtitle}</p>
        <div className="just-holder">
          <Link href="/contacts">
            <a className="just-btn hero__link btn">Schedule a call</a>
          </Link>
        </div>
        <a className="hero__btn" href="#main">
          <div style={{ width: "42", height: "42" }}>
            <svg
              width="62"
              height="33"
              viewBox="0 0 62 33"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M1.30151 1.69849L31 31.3969L60.6985 1.69849"
                stroke="white"
                strokeWidth="2"
              />
            </svg>
          </div>
        </a>
      </div>
    </div>
  );
}
