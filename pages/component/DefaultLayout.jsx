import React from "react";
import Header from "../component/header/Header";
import Footer from "../component/footer/footer";
import Home from "./Home";
import SiteIntro from "../component/SiteIntro";
export default function Layout({ children, href }) {
  // router.reload(window.location.pathname);

  return (
    <>
      <SiteIntro />

      {children}
      <Footer />
    </>
  );
}
