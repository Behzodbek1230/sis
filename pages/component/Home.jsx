/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useState } from "react";
import Header from "./header/Header";
import SiteHero from "./SiteHero";
import url from "../url.json";
import Carousel from "react-elastic-carousel";
import Link from "next/link";
import dateFormat, { masks } from "dateformat";
import useSWR from "swr";

export default function Home() {
  if (typeof window !== "undefined") {
    localStorage.setItem("rout", "/home");
  }
  const [datas, setData] = useState([]);
  const [partnor, setPartnor] = useState([]);

  useEffect(() => {
    fetch(url.url + "/newslist/")
      .then((res) => res.json())
      .then((data) => {
        setData(data);
      });
  }, []);
  useEffect(() => {
    fetch(url.url + "/partnor/")
      .then((res) => res.json())
      .then((data) => {
        setPartnor(data);
      });
  }, []);
  console.log(partnor);
  return (
    <>
      <SiteHero />

      <main className="main" id="main">
        <div className="container">
          <section className="company">
            <img
              className="company__img"
              src="img/main-img.jpeg"
              alt="main img"
              width="600"
              height="552"
            />
            <div className="company__info">
              <span className="company__info-desc section-feature">
                Mission
              </span>
              <h2 className="company__info-title section-title">
                Modern And Trusted Logistics Company
              </h2>
              <p className="company__info-text section-text">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Provident possimus quae adipisci quisquam distinctio nemo,
                tempora corrupti expedita nihil. Reiciendis impedit voluptates
                temporibus aut consectetur, vitae culpa et consectetur
                adipisicing elit. Provident possimus quae.
              </p>
              <ul className="company__info-list info-list">
                <li className="info__item">
                  <p className="info__text">24/7 Business Support</p>
                </li>
                <li className="info__item">
                  <p className="info__text">Secure Transportation</p>
                </li>
                <li className="info__item">
                  <p className="info__text">
                    World Wide Most Effective Business
                  </p>
                </li>
                <li className="info__item">
                  <p className="info__text">Easy And Quick Problem Analysis</p>
                </li>
              </ul>
            </div>
          </section>
        </div>

        <div className="news">
          <div className="container">
            <div className="news__detail">
              <span className="news__detail-feature section-feature">
                Relevant news
              </span>
              <h2 className="news__detail-title main-title">
                Make A Relation With Our Article And Meet With Our News
              </h2>
              <p className="news__detail-text">
                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Fugit
                porro, aliquid reprehenderit iusto inventore sint beatae ipsum
                impedit in sit illum distinctio sequi quisquam et hic tempore
              </p>
            </div>
            <article className="news__cards card">
              <Carousel itemsToShow={3}>
                {datas
                  ? datas.map((data, i) => (
                      <div
                        className="card__boxx"
                        style={{ marginBottom: "50px" }}
                        key={i}
                      >
                        <img
                          className="card__img"
                          src={data.image}
                          alt="card img"
                          width="410"
                          height="305"
                        />
                        <div className="card__body body-card">
                          <span className="body-card__date">
                            {dateFormat(data.date, "d mmmm  yyyy")}
                          </span>
                          <h3 className="body-card__title">{data.title}</h3>
                          <div className="body-card__text">
                            <div
                              dangerouslySetInnerHTML={{ __html: data.text }}
                            />
                          </div>
                          <Link href={"/news/" + data.id}>
                            <a className="body-card__link">Read More</a>
                          </Link>
                        </div>
                      </div>
                    ))
                  : ""}
              </Carousel>
            </article>
          </div>
        </div>

        <div className="partners">
          <div className="container">
            <h2 className="partners__title main-title">Our partners network</h2>
            <ul className="partners__list">
              {partnor.map((res) => (
                <li className="partners__item" key={res.id}>
                  <img
                    className="partners__img"
                    src={res.logo}
                    alt="logo"
                    width="300"
                    height="300"
                  />
                </li>
              ))}
            </ul>
          </div>
        </div>
      </main>
    </>
  );
}
