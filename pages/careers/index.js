/* eslint-disable @next/next/link-passhref */
/* eslint-disable react-hooks/rules-of-hooks */
import React, { useEffect, useState } from "react";
import Headers2 from "../component/header/Headers2";
import url from "../url.json";
import Link from "next/link";
export default function careers() {
  const [datas, setDatas] = useState([]);
  useEffect(() => {
    fetch(url.url + "/vacansy/")
      .then((res) => res.json())
      .then((data) => {
        setDatas(data);
      });
  }, []);
  return (
    <>
      <Headers2 />
      <div className="hero-page">
        <ul className="hero-page__list">
          <li className="hero-page__item">
            <a className="hero-page__link" href="#">
              Home
            </a>
          </li>
          <li className="hero-page__item">
            <a className="hero-page__link link--active" href="#">
              Careers
            </a>
          </li>
        </ul>
        <h2 className="hero-page__title title-page">Careers</h2>
      </div>
      <main className="main">
        <div className="container">
          <section className="careers">
            <ul className="careers__list">
              {datas.map((res) => (
                <Link href={"/careers/" + res.id} key={res.id}>
                  <li className="careers__item item-careers">
                    <span className="item-careers__tag section-feature">
                      {res.level}
                    </span>

                    <h3 className="item-careers__title section-title">
                      {res.title}
                    </h3>

                    <p className="item-careers__content">{res.body}</p>
                  </li>
                </Link>
              ))}
            </ul>
          </section>
        </div>
      </main>
    </>
  );
}
