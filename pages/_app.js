import "../public/scss/main.scss";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import DefaultLayout from "./component/DefaultLayout.jsx";

function MyApp({ Component, pageProps }) {
  const router = useRouter();

  useEffect(() => {
    if (router.pathname == "/") {
      if (localStorage.getItem("rout")) {
        router.push(localStorage.getItem("rout"));
      } else router.push("/home");
    }
  });
  return (
    <DefaultLayout>
      <Component {...pageProps} />
    </DefaultLayout>
  );
}

export default MyApp;
