/* eslint-disable @next/next/no-img-element */
/* eslint-disable react-hooks/rules-of-hooks */
import React, { useEffect, useState } from "react";
import url from "../url.json";
import { useRouter } from "next/router";
import Headers2 from "../component/header/Headers2";
import Date from "../../public/icons/date.svg";
import Trunc from "../../public/icons/truck.svg";
import Document from "../../public/icons/document.svg";
import Icons from "../../public/icons/icon.svg";
import Icons2 from "../../public/icons/icon2.svg";
import Icons3 from "../../public/icons/icon3.svg";
import Icons4 from "../../public/icons/icon4.svg";
export default function servic() {
  const router = useRouter();

  const servicesid = router.query.id;
  const [load, setLoading] = useState(true);
  const [datas, setDatas] = useState([]);

  const [ids, setids] = useState();
  useEffect(() => {
    const loadPost = async () => {
      setLoading(true);

      if (servicesid) {
        await fetch(`${url.url}/service_category/${servicesid}`)
          .then((res) => res.json())
          .then((data) => {
            setDatas(data);
            if (data.detail) router.push("/404");
          })
          .catch((error) => {
            router.push("/404");
          });
        setLoading(false);
      }
    };

    loadPost();
  }, [servicesid, datas]);

  if (load) {
    return <div>loading...</div>;
  } else
    return (
      <div>
        <Headers2 />
        <div className="hero-page">
          <ul className="hero-page__list">
            <li className="hero-page__item">
              <a className="hero-page__link" href="#">
                Home
              </a>
            </li>
            <li className="hero-page__item">
              <a className="hero-page__link link--active" href="#">
                Services
              </a>
            </li>
          </ul>
          <h2 className="hero-page__title title-page"> {datas.title}</h2>
        </div>
        <main className="main">
          <div className="container">
            <div className="reason">
              <div className="reason__inner">
                <div className="reason__detail">
                  <h2 className="reason__detail-title section-title">
                    {datas.title}
                  </h2>
                  <div className="reason__detail-text section-text">
                    <div dangerouslySetInnerHTML={{ __html: datas.body }} />
                  </div>
                  <ul className="reason__detail-slider slider">
                    <li className="slider__item">
                      <div className="slider__item-box">
                        <img
                          className="slider__item-icon"
                          src={Date}
                          alt="calendar icon"
                        />
                        <div></div>
                        <p className="slider__item-text">
                          20+ Years Work Experince
                        </p>
                      </div>
                    </li>
                    <li className="slider__item">
                      <div className="slider__item-box">
                        <img
                          className="slider__item-icon"
                          src={Trunc}
                          alt="truck icon"
                        />
                        <p className="slider__item-text">
                          World’s Area Covered
                        </p>
                      </div>
                    </li>
                    <li className="slider__item">
                      <div className="slider__item-box">
                        <img
                          className="slider__item-icon"
                          src={Document}
                          alt="document icon"
                        />
                        <p className="slider__item-text">
                          Corporate And Official Clients
                        </p>
                      </div>
                    </li>
                  </ul>
                </div>
                <div className="reason__holder">
                  <img
                    className="reason__holder-img reason-img"
                    src={url.img + datas.image}
                    alt="img"
                    width="600"
                    height="520"
                  />
                </div>
              </div>
            </div>
            <section className="advantage">
              <h3 className="advantage__title main-title">Advantages</h3>
              <div className="advantage__info">
                <div className="advantage__info-box box-info">
                  <img
                    className="box-info__img"
                    src={Icons}
                    alt="security icon"
                    width="96"
                    height="96"
                  />
                  <h3 className="box-info__title">100% Confidence & Quality</h3>
                  <p className="box-info__text">
                    We offer carrier certified products. Your devices arrive
                    fully tested, approved and activated
                  </p>
                </div>
                <div className="advantage__info-box box-info">
                  <img
                    className="box-info__img"
                    src={Icons2}
                    alt="security icon"
                    width="96"
                    height="96"
                  />
                  <h3 className="box-info__title">Fully Optimized</h3>
                  <p className="box-info__text">
                    We take full control of your fleet with practical, effective
                    tracking at a low cost.
                  </p>
                </div>
                <div className="advantage__info-box box-info">
                  <img
                    className="box-info__img"
                    src={Icons3}
                    alt="security icon"
                    width="96"
                    height="96"
                  />
                  <h3 className="box-info__title">Customer Satisfaction</h3>
                  <p className="box-info__text">
                    With our professional support you can find a GPS vehicle
                    tracking solution that matches your business needs
                  </p>
                </div>
                <div className="advantage__info-box box-info">
                  <img
                    className="box-info__img"
                    src={Icons4}
                    alt="security icon"
                    width="96"
                    height="96"
                  />
                  <h3 className="box-info__title">Best Pricing</h3>
                  <p className="box-info__text">
                    We offer you a free hardware, low cost - $25 per month
                    hardware tracker. 30 day money back guarantee
                  </p>
                </div>
              </div>
            </section>
            {datas.services.map((dat, i) => (
              <div key={i}>
                {i % 2 != 0 ? (
                  <section className="reason" key={i}>
                    <div className="reason__inner">
                      <div className="reason__holder">
                        <img
                          className="reason__holder-img reason-img"
                          src={url.img + dat.image}
                          alt="img"
                          width="600"
                          height="520"
                        />
                      </div>
                      <div className="reason__detail">
                        <h2 className="reason__detail-title section-title">
                          {dat.title}
                        </h2>
                        <div className="reason__detail-text section-text">
                          <div dangerouslySetInnerHTML={{ __html: dat.body }} />
                        </div>
                        <ul className="reason__detail-slider slider">
                          <li className="slider__item">
                            <div className="slider__item-box">
                              <img
                                className="slider__item-icon"
                                src={Date}
                                alt="calendar icon"
                              />
                              <p className="slider__item-text">
                                20+ Years Work Experince
                              </p>
                            </div>
                          </li>
                          <li className="slider__item">
                            <div className="slider__item-box">
                              <img
                                className="slider__item-icon"
                                src={Trunc}
                                alt="truck icon"
                              />
                              <p className="slider__item-text">
                                World’s Area Covered
                              </p>
                            </div>
                          </li>
                          <li className="slider__item">
                            <div className="slider__item-box">
                              <img
                                className="slider__item-icon"
                                src={Document}
                                alt="document icon"
                              />
                              <p className="slider__item-text">
                                Corporate And Official Clients
                              </p>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </section>
                ) : (
                  <section className="reason" key={i}>
                    <div className="reason__inner">
                      <div className="reason__detail">
                        <h2 className="reason__detail-title section-title">
                          {dat.title}
                        </h2>
                        <div className="reason__detail-text section-text">
                          <div dangerouslySetInnerHTML={{ __html: dat.body }} />
                        </div>
                        <ul className="reason__detail-slider slider">
                          <li className="slider__item">
                            <div className="slider__item-box">
                              <img
                                className="slider__item-icon"
                                src={Date}
                                alt="calendar icon"
                              />
                              <p className="slider__item-text">
                                20+ Years Work Experince
                              </p>
                            </div>
                          </li>
                          <li className="slider__item">
                            <div className="slider__item-box">
                              <img
                                className="slider__item-icon"
                                src={Trunc}
                                alt="truck icon"
                              />
                              <p className="slider__item-text">
                                World’s Area Covered
                              </p>
                            </div>
                          </li>
                          <li className="slider__item">
                            <div className="slider__item-box">
                              <img
                                className="slider__item-icon"
                                src={Document}
                                alt="document icon"
                              />
                              <p className="slider__item-text">
                                Corporate And Official Clients
                              </p>
                            </div>
                          </li>
                        </ul>
                      </div>
                      <div className="reason__holder">
                        <img
                          className="reason__holder-img reason-img"
                          src={url.img + dat.image}
                          alt="img"
                          width="600"
                          height="520"
                        />
                      </div>
                    </div>
                  </section>
                )}
              </div>
            ))}
          </div>
        </main>
      </div>
    );
}
