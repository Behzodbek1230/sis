/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useState } from "react";
import Headers2 from "../component/header/Headers2";
import url from "../url.json";
import Carousel from "react-elastic-carousel";
import Link from "next/link";
export default function services() {
  const [datas, setDatas] = useState([]);
  useEffect(() => {
    fetch(url.url + "/service_category/")
      .then((res) => res.json())
      .then((data) => {
        setDatas(data);
      });
  }, []);
  return (
    <>
      <Headers2 />
      <div className="hero-page">
        <ul className="hero-page__list">
          <li className="hero-page__item">
            <a className="hero-page__link" href="#">
              Home
            </a>
          </li>
          <li className="hero-page__item">
            <a className="hero-page__link link--active" href="#">
              Services
            </a>
          </li>
        </ul>
        <h2 className="hero-page__title title-page">Services</h2>
      </div>
      <main className="main">
        <div className="containerslider">
          <section className="service-card">
            <Carousel itemsToShow={4}>
              {datas
                ? datas.map((data, i) => (
                    <div
                      className="service-card__box"
                      key={i}
                      style={{ marginBottom: "50px" }}
                    >
                      <img
                        className="service-card__img"
                        src={data.image}
                        alt="service img"
                        width="300px"
                        height="300px"
                      />
                      <div className="service-card__body card-body">
                        <h3 className="card-body__title">{data.title}</h3>
                        <div dangerouslySetInnerHTML={{ __html: data.body }} />
                        <Link href={"/services/" + data.id}>
                          <a className="card-body__link">Read More</a>
                        </Link>
                      </div>
                    </div>
                  ))
                : ""}
            </Carousel>
          </section>
        </div>
      </main>
    </>
  );
}
