/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable @next/next/no-img-element */
import React, { useState, useEffect } from "react";
import Headers2 from "../component/header/Headers2";
import User from "../../public/icons/user.svg";
import Comment from "../../public/icons/comment.svg";
import Date from "../../public/icons/date.svg";
import Facebook from "../../public/icons/facebook-card.svg";
import Twitter from "../../public/icons/twitter-card.svg";
import url from "../url.json";
import { useRouter } from "next/router";
import dateFormat, { masks } from "dateformat";

export default function news() {
  const router = useRouter();

  const [load, setLoading] = useState(true);
  const [datas, setDatas] = useState([]);
  const [ids, setids] = useState();
  const newsid = router.query.newsId;
  useEffect(() => {
    setids(newsid);
    const loadPost = async () => {
      setLoading(true);

      if (newsid) {
        await fetch(`${url.url}/news/${newsid}`)
          .then((res) => res.json())
          .then((data) => {
            setDatas(data);
            if (data.detail) router.push("/404");
          })
          .catch((error) => {
            router.push("/404");
          });
        // Closed the loading page
        setLoading(false);
      }
    };

    loadPost();
  }, [newsid, datas]);

  if (load) {
    return <div>loading...</div>;
  } else
    return (
      <div>
        <Headers2 />
        <main className="main">
          <div className="container">
            <section className="card-open">
              <div className="card-open__wrapper card-wrapper">
                <div className="card-wrapper__holder">
                  <img
                    className="card-wrapper__img"
                    src={url.img + datas.image}
                    alt="card img"
                    width="1290"
                    height="390"
                  />
                </div>
                <div className="card-wrapper__body">
                  <ul className="card-body__list">
                    <li className="card-body__item">
                      <img
                        className="card-body__item-img"
                        src={User}
                        alt="user icon"
                        width="14"
                        height="14"
                      />
                      <p className="card-body__item-text">
                        Posted by: Carl Bradshaw
                      </p>
                    </li>
                    <li className="card-body__item">
                      <img
                        className="card-body__item-img"
                        src={Comment}
                        alt="user icon"
                        width="14"
                        height="14"
                      />
                      <p className="card-body__item-text">No comments</p>
                    </li>
                    <li className="card-body__item">
                      <img
                        className="card-body__item-img"
                        src={Date}
                        alt="user icon"
                        width="14"
                        height="14"
                      />
                      <p className="card-body__item-text">
                        {dateFormat(datas.date, "d mmmm  yyyy")}
                      </p>
                    </li>
                  </ul>
                  <div className="card-body__detail card-detail">
                    <h3 className="card-detail__title">{datas.title}</h3>
                    <div className="card-detail__wrapper">
                      <div className="card-detail__comment">
                        <ul className="card-detail__socials">
                          <li className="card-detail__socials-item">
                            <a
                              className="card-detail__socials-link facebook"
                              href="#"
                            >
                              <img
                                className="card-detail__socials-img"
                                src={Facebook}
                                alt="facebook logo"
                                width="15"
                                height="28"
                              />
                            </a>
                          </li>
                          <li className="card-detail__socials-item">
                            <a
                              className="card-detail__socials-link twitter"
                              href="#"
                            >
                              <img
                                className="card-detail__socials-img"
                                src={Twitter}
                                alt="twitter logo"
                                width="15"
                                height="28"
                              />
                            </a>
                          </li>
                        </ul>
                      </div>
                      <div className="card-detail__info">
                        <div
                          className="card-detail__info-text"
                          dangerouslySetInnerHTML={{ __html: datas.text }}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </main>
      </div>
    );
}
