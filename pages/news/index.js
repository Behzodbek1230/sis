/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable @next/next/no-img-element */
import React, { useEffect, useState } from "react";
import Headers2 from "../component/header/Headers2";
import url from "../url.json";
import dateFormat, { masks } from "dateformat";
import ReactPaginate from "react-paginate";
import { Button } from "reactstrap";
import Link from "next/link";
export default function news() {
  const [datas, setDatas] = useState([]);
  const [res, setRes] = useState([]);
  const [res1, setRes1] = useState([]);
  const [res2, setRes2] = useState([]);
  const [res3, setRes3] = useState([]);
  const [res4, setRes4] = useState([]);
  const [count, setCount] = useState();
  const [current, setcurrent] = useState(1);

  useEffect(() => {
    fetch(url.url + "/news/")
      .then((res) => res.json())
      .then((data) => {
        setDatas(data);
        setRes1(data.results[0]);
        setRes2(data.results[1]);
        setRes3(data.results[2]);
        setRes4(data.results[3]);
        setcurrent(1);
        data.count % 4 != 0
          ? setCount(Math.trunc(data.count / 4) + 1)
          : setCount(Math.trunc(data.count / 4));
      });
  }, []);
  function handlePageClick(index) {
    console.log(index);
    if (index > 1)
      fetch(url.url + "/news/?p=" + index)
        .then((res) => res.json())
        .then((data) => {
          console.log(data);
          setDatas(data);
          setRes1(data.results[0]);
          setRes2(data.results[1]);
          setRes3(data.results[2]);
          setRes4(data.results[3]);
          setcurrent(index);
        });
    else
      fetch(url.url + "/news/")
        .then((res) => res.json())
        .then((data) => {
          console.log(data);
          setDatas(data);
          setRes1(data.results[0]);
          setRes2(data.results[1]);
          setRes3(data.results[2]);
          setRes4(data.results[3]);
          setcurrent(index);
        });

    [];
  }

  return (
    <>
      <Headers2 />
      <div className="hero-page">
        <ul className="hero-page__list">
          <li className="hero-page__item">
            <a className="hero-page__link" href="#">
              Home
            </a>
          </li>
          <li className="hero-page__item">
            <a className="hero-page__link link--active" href="#">
              News
            </a>
          </li>
        </ul>
        <h2 className="hero-page__title title-page">News</h2>
      </div>
      <main className="main">
        <div className="container">
          <section className="news-card card">
            <div className="card__wrapper">
              {res1 ? (
                <div className="card__box box-card">
                  <div className="card__head">
                    <img
                      className="card__img"
                      src={res1.image}
                      alt="card img"
                      width="410"
                      height="305"
                    />
                  </div>
                  <div className="card__body body-card">
                    <span className="body-card__date">
                      {dateFormat(res1.date, "d mmmm  yyyy")}
                    </span>
                    <h3 className="body-card__title">{res1.title}</h3>
                    <div
                      className="body-card__text"
                      dangerouslySetInnerHTML={{ __html: res1.text }}
                    />
                    <Link href={"/news/" + res1.id}>
                      <a className="card-body__links">Read More</a>
                    </Link>
                  </div>
                </div>
              ) : (
                ""
              )}

              {res2 ? (
                <div className="card__box box-card">
                  <div className="card__head">
                    <img
                      className="card__img"
                      src={res2.image}
                      alt="card img"
                      width="410"
                      height="305"
                    />
                  </div>
                  <div className="card__body body-card">
                    <span className="body-card__date">
                      {dateFormat(res2.date, "d mmmm  yyyy")}
                    </span>
                    <h3 className="body-card__title">{res2.title}</h3>
                    <div
                      className="body-card__text"
                      dangerouslySetInnerHTML={{ __html: res2.text }}
                    />

                    <Link href={"/news/" + res2.id}>
                      <a className="card-body__links">Read More</a>
                    </Link>
                  </div>
                </div>
              ) : (
                ""
              )}
              {res3 ? (
                <div className="card__box box-card">
                  <div className="card__head">
                    <img
                      className="card__img"
                      src={res3.image}
                      alt="card img"
                      width="410"
                      height="305"
                    />
                  </div>
                  <div className="card__body body-card">
                    <span className="body-card__date">
                      {dateFormat(res3.date, "d mmmm  yyyy")}
                    </span>
                    <h3 className="body-card__title">{res3.title}</h3>
                    <div
                      className="body-card__text"
                      dangerouslySetInnerHTML={{ __html: res3.text }}
                    />

                    <Link href={"/news/" + res3.id}>
                      <a className="card-body__links">Read More</a>
                    </Link>
                  </div>
                </div>
              ) : (
                ""
              )}
              {res4 ? (
                <div className="card__box box-card">
                  <div className="card__head">
                    <img
                      className="card__img"
                      src={res4.image}
                      alt="card img"
                      width="410"
                      height="305"
                    />
                  </div>
                  <div className="card__body body-card">
                    <span className="body-card__date">
                      {dateFormat(res4.date, "d mmmm  yyyy")}
                    </span>
                    <h3 className="body-card__title">{res4.title}</h3>
                    <div
                      className="body-card__text"
                      dangerouslySetInnerHTML={{ __html: res4.text }}
                    />

                    <Link href={"/news/" + res4.id}>
                      <a className="card-body__links">Read More</a>
                    </Link>
                  </div>
                </div>
              ) : (
                ""
              )}
            </div>

            <ul className="pagination">
              {Array.from({ length: count }).map((currentelement, i) => (
                <li className="pagination__item" key={i}>
                  <a
                    className={
                      i + 1 == current
                        ? "pagination__link pagination__link--active"
                        : "pagination__link"
                    }
                    onClick={() => handlePageClick(i + 1)}
                  >
                    {i + 1}
                  </a>
                </li>
              ))}
            </ul>
          </section>
        </div>
      </main>
    </>
  );
}
