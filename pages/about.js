/* eslint-disable @next/next/no-img-element */
import React from "react";
import Headers2 from "./component/header/Headers2";
import SiteHero from "./component/SiteHero";

export default function about() {
  return (
    <>
      <Headers2 />

      <div className="hero-page">
        <ul className="hero-page__list">
          <li className="hero-page__item">
            <a className="hero-page__link" href="#">
              Home
            </a>
          </li>
          <li className="hero-page__item">
            <a className="hero-page__link link--active" href="#">
              About Us
            </a>
          </li>
        </ul>
        <h2 className="hero-page__title title-page">About Us</h2>
      </div>
      <main className="main">
        <div className="container">
          <section className="company">
            <img
              className="company__img"
              src="img/main-img.jpeg"
              alt="main img"
              width="600"
              height="552"
            />
            <div className="company__info">
              <span className="company__info-desc section-feature">
                Mission
              </span>
              <h2 className="company__info-title section-title">
                Modern And Trusted Logistics Company
              </h2>
              <p className="company__info-text section-text">
                Lorem ipsum dolor sit amen, consectetur adipisicing elit.
                Provident possimus quae adipisci quisquam distinctio nemo,
                tempora corrupti expedita nihil. Reiciendis impedit voluptates
                temporibus aut consectetur, vitae culpa et consectetur
                adipisicing elit. Provident possimus quae.
              </p>
              <ul className="company__info-list info-list">
                <li className="info__item">
                  <p className="info__text">24/7 Business Support</p>
                </li>
                <li className="info__item">
                  <p className="info__text">Secure Transportation</p>
                </li>
                <li className="info__item">
                  <p className="info__text">
                    World Wide Most Effective Business
                  </p>
                </li>
                <li className="info__item">
                  <p className="info__text">Easy And Quick Problem Analysis</p>
                </li>
              </ul>
            </div>
          </section>
          <section className="reason">
            <div className="reason__inner">
              <div className="reason__detail">
                <span className="reason__detail-feature section-feature">
                  Why Choose Us
                </span>
                <h2 className="reason__detail-title section-title">
                  We Are The Best And Thats Why You Can Choose Us Easily
                </h2>
                <p className="reason__detail-text section-text">
                  Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                  Fugit porro, aliquid reprehenderit iusto inventore sint beatae
                  ipsum impedit in sit illum distinctio sequi quisquam et hic
                  tempore Lorem ipsum dolor sit amet consectetur adipisicing
                  elit. Similique neque voluptatibus nam, cupiditate vero quos!
                  Quidem explicabo sed iste, aspernatur maiores ad, ipsum
                  consequatur et ipsa libero, temporibus ea magni? Lorem ipsum
                  dolor sit amet consectetur adipisicing elit. Voluptatibus
                  porro dolores dolor ducimus sapiente unde alias et cupiditate
                  ex, neque, ipsam suscipit animi dolorum enim nemo voluptates
                  ut ipsa vero voluptatem voluptatum facilis accusamus?
                  Laboriosam reiciendis iste excepturi est dicta!
                </p>
                <ul className="reason__detail-slider slider">
                  <li className="slider__item">
                    <div className="slider__item-box">
                      <div className="slider__item-icon">
                        <svg
                          width="28"
                          height="30"
                          viewBox="0 0 28 30"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M14.0001 24.3334C14.2638 24.3334 14.5216 24.2552 14.7408 24.1087C14.9601 23.9622 15.131 23.7539 15.2319 23.5103C15.3328 23.2666 15.3592 22.9986 15.3078 22.7399C15.2563 22.4813 15.1294 22.2437 14.9429 22.0572C14.7564 21.8708 14.5188 21.7438 14.2602 21.6923C14.0016 21.6409 13.7335 21.6673 13.4898 21.7682C13.2462 21.8691 13.038 22.04 12.8915 22.2593C12.7449 22.4785 12.6667 22.7363 12.6667 23C12.6667 23.3537 12.8072 23.6928 13.0573 23.9429C13.3073 24.1929 13.6465 24.3334 14.0001 24.3334ZM20.6667 24.3334C20.9305 24.3334 21.1882 24.2552 21.4075 24.1087C21.6268 23.9622 21.7977 23.7539 21.8986 23.5103C21.9995 23.2666 22.0259 22.9986 21.9745 22.7399C21.923 22.4813 21.796 22.2437 21.6096 22.0572C21.4231 21.8708 21.1855 21.7438 20.9269 21.6923C20.6682 21.6409 20.4001 21.6673 20.1565 21.7682C19.9129 21.8691 19.7046 22.04 19.5581 22.2593C19.4116 22.4785 19.3334 22.7363 19.3334 23C19.3334 23.3537 19.4739 23.6928 19.7239 23.9429C19.974 24.1929 20.3131 24.3334 20.6667 24.3334ZM20.6667 19C20.9305 19 21.1882 18.9218 21.4075 18.7753C21.6268 18.6288 21.7977 18.4206 21.8986 18.177C21.9995 17.9333 22.0259 17.6652 21.9745 17.4066C21.923 17.1479 21.796 16.9104 21.6096 16.7239C21.4231 16.5374 21.1855 16.4104 20.9269 16.359C20.6682 16.3075 20.4001 16.334 20.1565 16.4349C19.9129 16.5358 19.7046 16.7067 19.5581 16.9259C19.4116 17.1452 19.3334 17.403 19.3334 17.6667C19.3334 18.0203 19.4739 18.3595 19.7239 18.6095C19.974 18.8596 20.3131 19 20.6667 19ZM14.0001 19C14.2638 19 14.5216 18.9218 14.7408 18.7753C14.9601 18.6288 15.131 18.4206 15.2319 18.177C15.3328 17.9333 15.3592 17.6652 15.3078 17.4066C15.2563 17.1479 15.1294 16.9104 14.9429 16.7239C14.7564 16.5374 14.5188 16.4104 14.2602 16.359C14.0016 16.3075 13.7335 16.334 13.4898 16.4349C13.2462 16.5358 13.038 16.7067 12.8915 16.9259C12.7449 17.1452 12.6667 17.403 12.6667 17.6667C12.6667 18.0203 12.8072 18.3595 13.0573 18.6095C13.3073 18.8596 13.6465 19 14.0001 19ZM23.3334 3.00004H22.0001V1.66671C22.0001 1.31309 21.8596 0.973947 21.6096 0.723898C21.3595 0.47385 21.0204 0.333374 20.6667 0.333374C20.3131 0.333374 19.974 0.47385 19.7239 0.723898C19.4739 0.973947 19.3334 1.31309 19.3334 1.66671V3.00004H8.66675V1.66671C8.66675 1.31309 8.52627 0.973947 8.27622 0.723898C8.02618 0.47385 7.68704 0.333374 7.33342 0.333374C6.97979 0.333374 6.64065 0.47385 6.39061 0.723898C6.14056 0.973947 6.00008 1.31309 6.00008 1.66671V3.00004H4.66675C3.60588 3.00004 2.58847 3.42147 1.83832 4.17161C1.08818 4.92176 0.666748 5.93917 0.666748 7.00004V25.6667C0.666748 26.7276 1.08818 27.745 1.83832 28.4951C2.58847 29.2453 3.60588 29.6667 4.66675 29.6667H23.3334C24.3943 29.6667 25.4117 29.2453 26.1618 28.4951C26.912 27.745 27.3334 26.7276 27.3334 25.6667V7.00004C27.3334 5.93917 26.912 4.92176 26.1618 4.17161C25.4117 3.42147 24.3943 3.00004 23.3334 3.00004ZM24.6667 25.6667C24.6667 26.0203 24.5263 26.3595 24.2762 26.6095C24.0262 26.8596 23.687 27 23.3334 27H4.66675C4.31313 27 3.97399 26.8596 3.72394 26.6095C3.47389 26.3595 3.33341 26.0203 3.33341 25.6667V13.6667H24.6667V25.6667ZM24.6667 11H3.33341V7.00004C3.33341 6.64642 3.47389 6.30728 3.72394 6.05723C3.97399 5.80718 4.31313 5.66671 4.66675 5.66671H6.00008V7.00004C6.00008 7.35366 6.14056 7.6928 6.39061 7.94285C6.64065 8.1929 6.97979 8.33337 7.33342 8.33337C7.68704 8.33337 8.02618 8.1929 8.27622 7.94285C8.52627 7.6928 8.66675 7.35366 8.66675 7.00004V5.66671H19.3334V7.00004C19.3334 7.35366 19.4739 7.6928 19.7239 7.94285C19.974 8.1929 20.3131 8.33337 20.6667 8.33337C21.0204 8.33337 21.3595 8.1929 21.6096 7.94285C21.8596 7.6928 22.0001 7.35366 22.0001 7.00004V5.66671H23.3334C23.687 5.66671 24.0262 5.80718 24.2762 6.05723C24.5263 6.30728 24.6667 6.64642 24.6667 7.00004V11ZM7.33342 19C7.59712 19 7.85491 18.9218 8.07417 18.7753C8.29344 18.6288 8.46434 18.4206 8.56525 18.177C8.66617 17.9333 8.69258 17.6652 8.64113 17.4066C8.58968 17.1479 8.46269 16.9104 8.27622 16.7239C8.08975 16.5374 7.85218 16.4104 7.59354 16.359C7.33489 16.3075 7.06681 16.334 6.82317 16.4349C6.57954 16.5358 6.3713 16.7067 6.22479 16.9259C6.07828 17.1452 6.00008 17.403 6.00008 17.6667C6.00008 18.0203 6.14056 18.3595 6.39061 18.6095C6.64065 18.8596 6.97979 19 7.33342 19ZM7.33342 24.3334C7.59712 24.3334 7.85491 24.2552 8.07417 24.1087C8.29344 23.9622 8.46434 23.7539 8.56525 23.5103C8.66617 23.2666 8.69258 22.9986 8.64113 22.7399C8.58968 22.4813 8.46269 22.2437 8.27622 22.0572C8.08975 21.8708 7.85218 21.7438 7.59354 21.6923C7.33489 21.6409 7.06681 21.6673 6.82317 21.7682C6.57954 21.8691 6.3713 22.04 6.22479 22.2593C6.07828 22.4785 6.00008 22.7363 6.00008 23C6.00008 23.3537 6.14056 23.6928 6.39061 23.9429C6.64065 24.1929 6.97979 24.3334 7.33342 24.3334Z"
                            fill="#229ED9"
                          />
                        </svg>
                      </div>
                      <p className="slider__item-text">
                        20+ Years Work Experince
                      </p>
                    </div>
                  </li>
                  <li className="slider__item">
                    <div className="slider__item-box">
                      <div className="slider__item-icon">
                        <svg
                          width="30"
                          height="26"
                          viewBox="0 0 30 26"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M0.333252 13.6667V20.3334C0.333252 20.687 0.473728 21.0261 0.723776 21.2762C0.973825 21.5262 1.31296 21.6667 1.66659 21.6667H2.99992C2.99992 22.7276 3.42135 23.745 4.17149 24.4951C4.92164 25.2453 5.93905 25.6667 6.99992 25.6667C8.06078 25.6667 9.0782 25.2453 9.82835 24.4951C10.5785 23.745 10.9999 22.7276 10.9999 21.6667H18.9999C18.9999 22.7276 19.4213 23.745 20.1715 24.4951C20.9216 25.2453 21.9391 25.6667 22.9999 25.6667C24.0608 25.6667 25.0782 25.2453 25.8283 24.4951C26.5785 23.745 26.9999 22.7276 26.9999 21.6667H28.3333C28.6869 21.6667 29.026 21.5262 29.2761 21.2762C29.5261 21.0261 29.6666 20.687 29.6666 20.3334V4.33337C29.6666 3.27251 29.2452 2.25509 28.495 1.50495C27.7449 0.754801 26.7274 0.333374 25.6666 0.333374H13.6666C12.6057 0.333374 11.5883 0.754801 10.8382 1.50495C10.088 2.25509 9.66658 3.27251 9.66658 4.33337V7.00004H6.99992C6.37894 7.00004 5.76649 7.14462 5.21106 7.42233C4.65564 7.70004 4.17251 8.10326 3.79992 8.60004L0.599919 12.8667C0.56092 12.9247 0.529551 12.9874 0.506585 13.0534L0.426585 13.2C0.36774 13.3487 0.336125 13.5068 0.333252 13.6667ZM21.6666 21.6667C21.6666 21.403 21.7448 21.1452 21.8913 20.9259C22.0378 20.7067 22.246 20.5358 22.4897 20.4349C22.7333 20.334 23.0014 20.3075 23.26 20.359C23.5187 20.4104 23.7563 20.5374 23.9427 20.7239C24.1292 20.9104 24.2562 21.1479 24.3076 21.4066C24.3591 21.6652 24.3327 21.9333 24.2318 22.177C24.1308 22.4206 23.9599 22.6288 23.7407 22.7753C23.5214 22.9218 23.2636 23 22.9999 23C22.6463 23 22.3072 22.8596 22.0571 22.6095C21.8071 22.3595 21.6666 22.0203 21.6666 21.6667ZM12.3333 4.33337C12.3333 3.97975 12.4737 3.64061 12.7238 3.39057C12.9738 3.14052 13.313 3.00004 13.6666 3.00004H25.6666C26.0202 3.00004 26.3593 3.14052 26.6094 3.39057C26.8594 3.64061 26.9999 3.97975 26.9999 4.33337V19H25.9599C25.585 18.5876 25.128 18.258 24.6183 18.0325C24.1086 17.807 23.5573 17.6905 22.9999 17.6905C22.4425 17.6905 21.8913 17.807 21.3815 18.0325C20.8718 18.258 20.4148 18.5876 20.0399 19H12.3333V4.33337ZM9.66658 12.3334H4.33325L5.93325 10.2C6.05745 10.0344 6.21849 9.90004 6.40363 9.80747C6.58877 9.7149 6.79292 9.66671 6.99992 9.66671H9.66658V12.3334ZM5.66658 21.6667C5.66658 21.403 5.74478 21.1452 5.89129 20.9259C6.0378 20.7067 6.24604 20.5358 6.48967 20.4349C6.73331 20.334 7.0014 20.3075 7.26004 20.359C7.51868 20.4104 7.75626 20.5374 7.94273 20.7239C8.1292 20.9104 8.25619 21.1479 8.30763 21.4066C8.35908 21.6652 8.33267 21.9333 8.23176 22.177C8.13084 22.4206 7.95994 22.6288 7.74068 22.7753C7.52141 22.9218 7.26363 23 6.99992 23C6.6463 23 6.30716 22.8596 6.05711 22.6095C5.80706 22.3595 5.66658 22.0203 5.66658 21.6667ZM2.99992 15H9.66658V18.7067C8.87969 18.0034 7.84676 17.6397 6.79284 17.6946C5.73892 17.7495 4.74942 18.2188 4.03992 19H2.99992V15Z"
                            fill="#229ED9"
                          />
                        </svg>
                      </div>

                      <p className="slider__item-text">World’s Area Covered</p>
                    </div>
                  </li>
                  <li className="slider__item">
                    <div className="slider__item-box">
                      <div className="slider__item-icon">
                        <svg
                          width="28"
                          height="28"
                          viewBox="0 0 28 28"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M7.33342 16.6666H10.0001C10.3537 16.6666 10.6928 16.5262 10.9429 16.2761C11.1929 16.0261 11.3334 15.6869 11.3334 15.3333C11.3334 14.9797 11.1929 14.6405 10.9429 14.3905C10.6928 14.1404 10.3537 14 10.0001 14H7.33342C6.97979 14 6.64065 14.1404 6.39061 14.3905C6.14056 14.6405 6.00008 14.9797 6.00008 15.3333C6.00008 15.6869 6.14056 16.0261 6.39061 16.2761C6.64065 16.5262 6.97979 16.6666 7.33342 16.6666ZM15.3334 19.3333H7.33342C6.97979 19.3333 6.64065 19.4738 6.39061 19.7238C6.14056 19.9739 6.00008 20.313 6.00008 20.6666C6.00008 21.0202 6.14056 21.3594 6.39061 21.6094C6.64065 21.8595 6.97979 22 7.33342 22H15.3334C15.687 22 16.0262 21.8595 16.2762 21.6094C16.5263 21.3594 16.6667 21.0202 16.6667 20.6666C16.6667 20.313 16.5263 19.9739 16.2762 19.7238C16.0262 19.4738 15.687 19.3333 15.3334 19.3333ZM23.3334 0.666626H4.66675C3.60588 0.666626 2.58847 1.08805 1.83832 1.8382C1.08818 2.58834 0.666748 3.60576 0.666748 4.66663V23.3333C0.666748 24.3942 1.08818 25.4116 1.83832 26.1617C2.58847 26.9119 3.60588 27.3333 4.66675 27.3333H23.3334C24.3943 27.3333 25.4117 26.9119 26.1618 26.1617C26.912 25.4116 27.3334 24.3942 27.3334 23.3333V4.66663C27.3334 3.60576 26.912 2.58834 26.1618 1.8382C25.4117 1.08805 24.3943 0.666626 23.3334 0.666626ZM16.6667 3.33329V7.71996L14.6534 6.59996C14.4507 6.48293 14.2208 6.42133 13.9867 6.42133C13.7527 6.42133 13.5228 6.48293 13.3201 6.59996L11.3334 7.71996V3.33329H16.6667ZM24.6667 23.3333C24.6667 23.6869 24.5263 24.0261 24.2762 24.2761C24.0262 24.5262 23.687 24.6666 23.3334 24.6666H4.66675C4.31313 24.6666 3.97399 24.5262 3.72394 24.2761C3.47389 24.0261 3.33341 23.6869 3.33341 23.3333V4.66663C3.33341 4.313 3.47389 3.97387 3.72394 3.72382C3.97399 3.47377 4.31313 3.33329 4.66675 3.33329H8.66675V9.99996C8.66815 10.2326 8.73043 10.4609 8.84738 10.6621C8.96433 10.8632 9.1319 11.0303 9.33342 11.1466C9.53611 11.2636 9.76603 11.3253 10.0001 11.3253C10.2341 11.3253 10.4641 11.2636 10.6667 11.1466L14.0001 9.29329L17.3467 11.16C17.546 11.2729 17.771 11.3326 18.0001 11.3333C18.3537 11.3333 18.6928 11.1928 18.9429 10.9428C19.1929 10.6927 19.3334 10.3536 19.3334 9.99996V3.33329H23.3334C23.687 3.33329 24.0262 3.47377 24.2762 3.72382C24.5263 3.97387 24.6667 4.313 24.6667 4.66663V23.3333Z"
                            fill="#229ED9"
                          />
                        </svg>
                      </div>

                      <p className="slider__item-text">
                        Corporate And Official Clients
                      </p>
                    </div>
                  </li>
                </ul>
              </div>
              <div className="reason__holder">
                <img
                  className="reason__holder-img"
                  src="img/hero-img.jpeg"
                  alt="img"
                  width="600"
                  height="520"
                />
              </div>
            </div>
          </section>
          <section className="trust">
            <div className="trust__inner">
              <div className="trust__holder">
                <img
                  className="trust__holder-img"
                  src="img/hero-bg.jpeg"
                  alt="img"
                  width="600"
                  height="520"
                />
              </div>
              <div className="trust__detail">
                <h2 className="trust__detail-title section-title">
                  Modern And Trusted Logistics Company
                </h2>
                <p className="trust__detail-text section-text">
                  Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                  Provident, dolorem, unde commodi assumenda qui ducimus
                  eveniet, placeat nostrum maiores ab soluta odio exercitationem
                  temporibus omnis!
                </p>
                <p className="trust__detail-text section-text">
                  Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                  Provident, dolorem, unde commodi assumenda qui ducimus
                  eveniet, placeat nostrum maiores ab soluta odio exercitationem
                  temporibus omnis!
                </p>
                <p className="trust__detail-text section-text">
                  Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                  Provident, dolorem, unde commodi assumenda qui ducimus
                  eveniet, placeat nostrum maiores ab soluta odio exercitationem
                  temporibus omnis!
                </p>
                <p className="trust__detail-text section-text">
                  Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                  Provident, dolorem, unde commodi assumenda qui ducimus
                  eveniet, placeat nostrum maiores ab soluta odio exercitationem
                  temporibus omnis!
                </p>
              </div>
            </div>
          </section>
        </div>
      </main>
    </>
  );
}
